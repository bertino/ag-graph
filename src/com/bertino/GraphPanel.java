package com.bertino;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import java.util.Vector;

public class GraphPanel extends JPanel {
    Node startNode = null;
    Node movedNode = null;
    Point endPoint = null;
    boolean isDragging = false;
    private Graph graph;

    Node fordStart = null;
    Node fordEnd = null;

    public GraphPanel() {
        this(new Graph());
        graph.setPanel(this);
    }

    public GraphPanel(Graph graph) {
        this.graph = graph;

        JCheckBox undirectedCheckbox = new JCheckBox("Graf neorientat?");
        add(undirectedCheckbox);

        JButton generateButton = new JButton("Generează graf aleatoriu");
        add(generateButton);

        JButton clearButton = new JButton("Resetează graful");
        add(clearButton);

        JButton printTopoligicalButton = new JButton("Calculează sortarea topologică");
        add(printTopoligicalButton);

        JButton connectedComponentsButton = new JButton("Componente conexe");
        add(connectedComponentsButton);

        JButton kruskalButton = new JButton("Kruskal");
        add(kruskalButton);

        JButton primButton = new JButton("Prim");
        add(primButton);

        // borderul panel-ului
        setBorder(BorderFactory.createLineBorder(Color.black));
        undirectedCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                JCheckBox cb = (JCheckBox) event.getSource();
                if (cb.isSelected()) {
                    graph.makeUndirected();
                } else {
                    graph.makeDirected();
                }
            }
        });

        generateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                JTextField numNodesField = new JTextField(Graph.nodesToGenerate + "");
                JTextField probabilityField = new JTextField(Graph.edgeProbability + "");
                Object[] message = { "Număr de noduri:", numNodesField, "Probabilitate arc:", probabilityField };
                JOptionPane.showMessageDialog(null, message);
                try {
                    int newNodes = Integer.parseInt(numNodesField.getText());
                    if (newNodes != 0 && newNodes <= 500)
                        Graph.nodesToGenerate = newNodes;

                    double newProbability = Double.parseDouble(probabilityField.getText());
                    if (newProbability != 0)
                        Graph.edgeProbability = newProbability;

                    graph.generateGraph(newNodes);
                } catch (NumberFormatException e) {
                }
            }
        });

        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                graph.clearGraph();
            }
        });

        printTopoligicalButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Sortarea topologică este: ");
                for (Node n : graph.topologicalSort())
                    System.out.print(n.getNumber());
                System.out.print("\n");
            }
        });

        connectedComponentsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Componentele (tare) conexe sunt: ");
                Vector<Vector<Node>> components = graph.stronglyConnectedComponents();
                System.out.println(components);

                if (graph.directed)
                    graph.collapseComponents(components);
                else
                    colorComponents(components);
            }
        });

        kruskalButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Costul arborelui este: " + graph.kruskalMST());
            }
        });

        primButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.out.println("Costul arborelui este: " + graph.kruskalMST());
            }
        });

        addMouseListener(new MouseAdapter() {
            // evenimentul care se produce la apasarea mouse-ului
            public void mousePressed(MouseEvent ev) {
                for (Node n : graph.getNodes())
                    if (n.collidesWith(ev.getPoint()))
                        if (ev.getButton() == MouseEvent.BUTTON1)
                            startNode = n;
                        else if (ev.getButton() == MouseEvent.BUTTON3)
                            movedNode = n;
                        else {
                            if (fordStart == null) {
                                fordStart = n;
                                n.setColor(n.getColor().brighter());
                            } else {
                                if (fordEnd == null) {
                                    System.out.println(graph.fordFulkerson(fordStart, n));
                                    n.setColor(n.getColor().brighter());
                                    fordEnd = n;
                                } else {
                                    fordStart.setColor(Node.defaultColor);
                                    fordEnd.setColor(Node.defaultColor);
                                    fordStart = n;
                                    fordEnd = null;
                                    n.setColor(n.getColor().brighter());
                                }
                            }
                        }

                for (Edge e : graph.getEdges())
                    if (e.clicked(ev.getPoint()) && ev.getButton() == MouseEvent.BUTTON1) {
                        JTextField capacityField = new JTextField(e.getCapacity() + "");
                        JTextField flowField = new JTextField("0");

                        Object[] message = { "Capacitate:", capacityField, "Flux:", flowField };
                        JOptionPane.showMessageDialog(null, message);

                        int capacity = Integer.parseInt(capacityField.getText());
                        int flow = Integer.parseInt(flowField.getText());

                        flow = Math.min(flow, capacity);

                        e.setCapacity(capacity);
                        e.setFlow(flow);
                    }
            }

            // evenimentul care se produce la eliberarea mouse-ului
            public void mouseReleased(MouseEvent ev) {
                if (!isDragging) {
                    graph.addNode(ev.getPoint());
                } else if (startNode != null)
                    for (Node n : graph.getNodes())
                        if (n.collidesWith(ev.getPoint())) {
                            graph.addEdge(startNode, n);

                            if (!graph.directed)
                                graph.addEdge(n, startNode);
                        }
                startNode = null;
                movedNode = null;
                isDragging = false;
                repaint();
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            // evenimentul care se produce la drag&drop pe mouse
            public void mouseDragged(MouseEvent ev) {
                endPoint = ev.getPoint();
                isDragging = true;

                if (movedNode != null)
                    movedNode.setCenter(new Point(ev.getPoint()));

                repaint();
            }
        });
    }

    public void transposeGraph() {
        graph = graph.getTranspose();
        graph.writeToFile();
        repaint();
    }

    private void colorComponents(Vector<Vector<Node>> components) {
        for (int i = 0; i < components.size(); i++) {
            Vector<Node> component = components.get(i);

            float hue = (float) i / components.size();
            System.out.println(hue);
            Color color = Color.getHSBColor(hue, 0.75f, 1.0f);

            for (Node n : component)
                n.setColor(color);
        }
        repaint();
    }

    // se executa atunci cand apelam repaint()
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);// apelez metoda paintComponent din clasa de baza

        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        for (Edge e : graph.getEdges())
            e.drawArc(g);

        // deseneaza arcul curent; cel care e in curs de desenare
        if (startNode != null) {
            g.setColor(Color.getHSBColor(1.0f, 0.75f, 1.0f));
            Edge.drawArrow(g, startNode.getCenter(), endPoint);
        }

        // deseneaza lista de noduri
        for (Node n : graph.getNodes())
            n.drawNode(g);
    }
}
