package com.bertino;

import java.awt.Point;

import java.nio.file.Files;
import java.nio.file.Path;
import java.io.IOException;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeSet;
import java.util.Random;
import java.util.Vector;
import java.util.Stack;

public class Graph {
    private Vector<Node> nodes;
    private Vector<Edge> edges;
    public boolean directed = true;
    public static double edgeProbability = 0.3;
    public static int nodesToGenerate = 5;

    private GraphPanel panel;

    public Vector<Node> getNodes() {
        return nodes;
    }

    public Vector<Edge> getEdges() {
        return edges;
    }

    public GraphPanel getPanel() {
        return panel;
    }

    public void setPanel(GraphPanel panel) {
        this.panel = panel;
    }

    public Graph shallowCopy() {
        Graph newGraph = new Graph(panel);
        newGraph.edges = new Vector<Edge>(edges);
        newGraph.nodes = new Vector<Node>(nodes);
        return newGraph;
    }

    public Graph() {
        this(null);
    }

    public Graph(GraphPanel panel) {
        nodes = new Vector<Node>();
        edges = new Vector<Edge>();
        this.panel = panel;
    }

    private void dfsUtil(Node n, boolean visited[], Stack<Node> stack) {
        visited[n.getNumber()] = true;

        for (Node neighbor : n.getNeighbors())
            if (!visited[neighbor.getNumber()])
                dfsUtil(neighbor, visited, stack);

        if (stack != null)
            stack.push(n);
    }

    void dfsUtil(int nodeIndex, boolean visited[], Stack<Integer> stack) {
        Node n = nodes.get(nodeIndex);

        visited[nodeIndex] = true;

        for (Node neighbor : n.getNeighbors())
            if (!visited[neighbor.getNumber()])
                dfsUtil(neighbor.getNumber(), visited, stack);

        if (stack != null)
            stack.push(n.getNumber());
    }

    public Vector<Node> topologicalSort() {
        Stack<Node> stack = new Stack<Node>();

        boolean visited[] = new boolean[nodes.size()];
        for (int i = 0; i < visited.length; i++)
            visited[i] = false;

        for (Node n : nodes)
            if (!visited[n.getNumber()])
                dfsUtil(n, visited, stack);

        Collections.reverse(stack);
        return stack;
    }

    public Graph getTranspose() {
        Graph transposed = new Graph();
        transposed.nodes = new Vector<Node>();
        transposed.edges = new Vector<Edge>();

        for (Node n : nodes)
            transposed.addNode(n.getCenter());

        for (Edge e : edges) {
            Node start = transposed.nodes.get(e.getEnd().getNumber());
            Node end = transposed.nodes.get(e.getStart().getNumber());

            transposed.addEdge(start, end);
        }

        return transposed;
    }

    public Vector<Vector<Node>> stronglyConnectedComponents() {
        Vector<Vector<Node>> components = new Vector<Vector<Node>>();

        Stack<Integer> stack = new Stack<Integer>();

        boolean visited[] = new boolean[nodes.size()];
        for (int i = 0; i < visited.length; i++)
            visited[i] = false;

        // Fill vertices in stack according to their finishing
        // times
        for (Node n : nodes)
            if (!visited[n.getNumber()])
                dfsUtil(n.getNumber(), visited, stack);

        // Create a reversed graph
        Graph transposed = getTranspose();

        // Mark all the vertices as not visited (For second DFS)
        for (int i = 0; i < visited.length; i++)
            visited[i] = false;

        // Now process all vertices in order defined by Stack
        while (!stack.empty()) {
            // Pop a vertex from stack
            Node n = transposed.nodes.get(stack.pop());
            // System.out.println(n);

            // Print Strongly connected component of the popped vertex
            if (visited[n.getNumber()] == false) {
                Stack<Node> transposedComponent = new Stack<Node>();
                Vector<Node> component = new Vector<Node>();
                transposed.dfsUtil(n, visited, transposedComponent);
                Collections.reverse(transposedComponent);
                for (Node element : transposedComponent)
                    component.add(nodes.get(element.getNumber()));
                components.add(component);
            }
        }

        return components;
    }

    private int findComponentOf(Node n, Vector<Vector<Node>> components) {
        for (int i = 0; i < components.size(); i++)
            if (components.get(i).contains(n))
                return i;
        return -1;
    }

    public void collapseComponents() {
        collapseComponents(stronglyConnectedComponents());
    }

    public void collapseComponents(Vector<Vector<Node>> components) {
        if (components.size() == nodes.size())
            return;

        Graph old = shallowCopy();
        old.setPanel(null);

        clearGraph();

        for (int i = 0; i < components.size(); i++) {
            Vector<Node> component = components.get(i);

            addNode(component.get(0).getCenter());
            String label = "";

            for (int j = 0; j < component.size() - 1; j++)
                label += component.get(j) + "+";
            label += component.lastElement();

            getNodes().lastElement().setLabel(label);
        }

        for (Edge e : old.getEdges()) {
            int startComponentIndex = findComponentOf(e.getStart(), components);
            int endComponentIndex = findComponentOf(e.getEnd(), components);

            if (startComponentIndex == -1 || endComponentIndex == -1)
                return;

            Node start = getNodes().get(startComponentIndex);
            Node end = getNodes().get(endComponentIndex);

            addEdge(start, end);
        }

        if (panel != null)
            panel.repaint();
    }

    private class DSU {
        HashMap<Node, Node> parentOf;
        HashMap<Node, Integer> rankOf;

        DSU(Vector<Node> nodes) {
            parentOf = new HashMap<Node, Node>();
            rankOf = new HashMap<Node, Integer>();

            for (Node n : nodes) {
                parentOf.put(n, null);
                rankOf.put(n, 1);
            }
        }

        public Node findParent(Node n) {
            if (parentOf.get(n) == null)
                return n;

            return findParent(parentOf.get(n));
        }

        public void unite(Node n1, Node n2) {
            Node s1 = findParent(n1);
            Node s2 = findParent(n2);

            if (s1 != s2)
                if (rankOf.get(s1) < rankOf.get(s2)) {
                    parentOf.put(s1, s2);
                    int newRank = rankOf.get(s1) + rankOf.get(s2);
                    rankOf.put(s2, newRank);
                } else {
                    parentOf.put(s2, s1);
                    int newRank = rankOf.get(s1) + rankOf.get(s2);
                    rankOf.put(s1, newRank);
                }

        }
    }

    public int kruskalMST() {
        Vector<Edge> finalEdges = new Vector<Edge>();
        Collections.sort(edges);
        int totalWeight = 0;

        DSU dsu = new DSU(nodes);

        for (Edge e : edges)
            if (dsu.findParent(e.getStart()) != dsu.findParent(e.getEnd())) {
                dsu.unite(e.getStart(), e.getEnd());
                finalEdges.add(e);
                totalWeight += e.getCapacity();
            }

        edges = finalEdges;

        if (panel != null)
            panel.repaint();

        return totalWeight;
    }

    class KeyComparator implements Comparator<Node> {
        HashMap<Node, Integer> keys = new HashMap<Node, Integer>();

        KeyComparator(HashMap<Node, Integer> keys) {
            this.keys = keys;
        }

        public int compare(Node n1, Node n2) {
            return keys.get(n1) - keys.get(n2);
        }
    }

    public int primMST() {
        int totalWeight = 0;
        HashMap<Node, Node> parentOf = new HashMap<Node, Node>();
        Vector<Edge> finalEdges = new Vector<Edge>();
        HashMap<Node, Integer> keys = new HashMap<Node, Integer>();

        KeyComparator comp = new KeyComparator(keys);
        TreeSet<Node> notIncluded = new TreeSet<Node>(comp);

        for (Node n : nodes) {
            notIncluded.add(n);
            keys.put(n, Integer.MAX_VALUE);
            parentOf.put(n, nodes.get(0));
        }

        notIncluded.remove(nodes.get(0));
        keys.put(nodes.get(0), 0);
        parentOf.put(nodes.get(0), null);

        for (int i = 0; i < nodes.size(); i++) {
            Node min = notIncluded.first();

            for (Edge e : min.getEdges()) {
                Node neighbor = e.getEnd();
                if (!notIncluded.contains(neighbor) && e.getCapacity() < keys.get(neighbor)) {
                    parentOf.put(neighbor, min);
                    keys.put(neighbor, e.getCapacity());
                }
            }
        }

        Vector<Edge> oldEdges = new Vector<Edge>(edges);
        edges.clear();

        for (Node n : nodes) {
            for (Node neighbor : n.getNeighbors())
                neighbor.clearNeighbors();
            n.clearNeighbors();

            if (parentOf.get(n) != null) {
                addEdge(n, parentOf.get(n));
                addEdge(parentOf.get(n), n);

                totalWeight += edges.lastElement().getCapacity();
            }
        }

        if (panel != null)
            panel.repaint();

        return totalWeight;
    }

    private boolean residualBfs(Node start, Node end, HashMap<Node, Edge> parentOf) {
        for (Node n : nodes)
            parentOf.put(n, null);

        HashMap<Node, Boolean> visited = new HashMap<>();
        for (Node n : nodes)
            visited.put(n, false);

        Queue<Node> q = new LinkedList<>();

        q.add(start);
        visited.put(start, true);

        while (!q.isEmpty()) {
            Node current = q.poll();

            for (Edge e : current.getEdges())
                if (e.getCapacity() > e.getFlow() && !visited.get(e.getEnd())) {
                    Node neighbor = e.getEnd();

                    parentOf.put(neighbor, e);

                    if (neighbor == end)
                        return true;

                    q.add(neighbor);
                    visited.put(neighbor, true);
                }
        }

        return false;
    }

    private Edge reverseEdge(Edge e) {
        for (Edge reverse : e.getEnd().getEdges())
            if (e.getStart() == reverse.getEnd())
                return reverse;

        return null;
    }

    public int fordFulkerson(Node start, Node end) {
        HashMap<Node, Edge> parentEdges = new HashMap<>();

        int maxFlow = 0;

        while (residualBfs(start, end, parentEdges)) {
            int pathFlow = Integer.MAX_VALUE;

            for (Edge e = parentEdges.get(end); e != null; e = parentEdges.get(e.getStart()))
                pathFlow = Math.min(pathFlow, e.getCapacity() - e.getFlow());

            for (Edge e = parentEdges.get(end); e != null; e = parentEdges.get(e.getStart())) {
                e.setFlow(e.getFlow() + pathFlow);
                Edge reverse = reverseEdge(e);
                if (reverse != null)
                    reverse.setFlow(reverse.getFlow() - pathFlow);
            }

            maxFlow += pathFlow;
        }

        return maxFlow;
    }

    public void makeDirected() {
        directed = true;
        Edge.arrowSize = Edge.defaultArrowSize;
        if (panel != null)
            panel.repaint();
    }

    public void makeUndirected() {
        directed = false;
        Edge.arrowSize = 0;

        Vector<Edge> oldEdges = new Vector<Edge>(edges);
        edges.clear();

        for (Edge e : oldEdges) {
            addEdge(e.getStart(), e.getEnd());
            addEdge(e.getEnd(), e.getStart());
        }

        writeToFile();
        if (panel != null)
            panel.repaint();
    }

    public void generateGraph(int numNodes) {
        Random random = new Random();
        int maxWidth = 500, maxHeight = 500;
        if (panel != null) {
            maxWidth = panel.getWidth();
            maxHeight = panel.getHeight();
        }

        int x, y, initialNodes = nodes.size();
        long startTime = System.currentTimeMillis();
        boolean added;
        System.out.println("Number of nodes to add: " + numNodes);
        for (int i = 0; i < numNodes; i++) {
            added = false;
            for (int tries = 0; tries < 10; tries++) {
                x = random.nextInt(maxWidth);
                y = random.nextInt(maxHeight);
                if (addNode(new Point(x, y))) {
                    added = true;
                    break;
                }
            }
            if (!added)
                break;
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Managed to add " + (nodes.size() - initialNodes) + " nodes in "
                + ((endTime - startTime) / 100.0) + " seconds.");

        for (Node n1 : nodes)
            for (Node n2 : nodes)
                if (random.nextDouble() < edgeProbability)
                    addEdge(n1, n2);

    }

    public void clearGraph() {
        edges.clear();
        nodes.clear();
        if (panel != null)
            panel.repaint();
    }

    public void writeToFile() {
        Path filePath = Path.of("graph.txt");
        Vector<String> output = new Vector<String>();
        try {
            output.add(nodes.size() + "");
            for (Node n : nodes) {
                int[] neighbors = new int[nodes.size()];
                Arrays.fill(neighbors, 0);
                for (Node neighbor : n.getNeighbors())
                    neighbors[neighbor.getNumber()] = 1;

                String line = "";
                for (int x : neighbors)
                    line += x + " ";
                output.add(line);
            }
            Files.write(filePath, output);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    // metoda care se apeleaza la eliberarea mouse-ului
    public boolean addNode(Point center) {
        for (Node n : nodes)
            if (n.collidesWith(center))
                return false;

        Node node = new Node(center, nodes.size());
        nodes.add(node);
        writeToFile();

        if (panel != null)
            panel.repaint();
        return true;
    }

    public void addEdge(Node start, Node end) {
        addEdge(start, end, null);
    }

    public void addEdge(Node start, Node end, Integer cost) {
        for (Edge e : edges)
            if (e.getStart() == start && e.getEnd() == end)
                return;

        if (start == end)
            return;

        if (cost != null)
            edges.add(new Edge(start, end, cost));
        else
            edges.add(new Edge(start, end));

        writeToFile();
    }
}
