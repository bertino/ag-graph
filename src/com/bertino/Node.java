package com.bertino;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

public class Node {
    public static int radius = 15;
    private Point center;
    private Vector<Edge> edges;
    private int number;
    private String label;
    public static Color defaultColor = Color.getHSBColor(1.0f, 0.75f, 1.0f);
    private Color color = defaultColor;

    public Node(Point center, int number) {
        this.setCenter(center);
        this.number = number;
        this.edges = new Vector<Edge>();
        this.label = ((Integer) number).toString();
    }

    public Node(int x, int y, int number) {
        this.setCenter(new Point(x, y));
        this.number = number;
    }

    public boolean collidesWith(Point p) {
        return center.distance(p) <= radius * 2;
    }

    public void addEdge(Edge e) {
        edges.add(e);
    }

    public Vector<Edge> getEdges() {
        return edges;
    }

    public Vector<Node> getNeighbors() {
        Vector<Node> neighbors = new Vector<Node>();

        for (Edge e : edges)
            neighbors.add(e.getEnd());

        return neighbors;
    }

    public void clearNeighbors() {
        edges.clear();
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public int getNumber() {
        return number;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void drawNode(Graphics g) {
        g.setColor(color);
        g.fillOval(center.x - radius, center.y - radius, radius * 2, radius * 2);

        g.setColor(Color.BLACK);
        g.drawOval(center.x - radius, center.y - radius, radius * 2, radius * 2);

        g.setFont(new Font("TimesRoman", Font.BOLD, radius));
        g.drawString(label, center.x - label.length() * radius / 3, center.y + radius / 3);
    }

    public String toString() {
        return label;
    }
}
