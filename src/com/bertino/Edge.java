package com.bertino;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.*;

public class Edge implements Comparable<Edge> {
    public static int arrowSize = 8;
    public static int defaultArrowSize = 8;

    private Node start;
    private Node end;
    private int capacity;
    private int flow;

    public int compareTo(Edge other) {
        return this.capacity - other.capacity;
    }

    public Edge(Node start, Node end) {
        this(start, end, (int) (start.getCenter().distance(end.getCenter()) / 10));
    }

    public Edge(Node start, Node end, int capacity) {
        this.start = start;
        this.end = end;
        this.capacity = capacity;
        this.flow = 0;
        start.addEdge(this);
    }

    public Node getStart() {
        return start;
    }

    public Node getEnd() {
        return end;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getFlow() {
        return flow;
    }

    public void setFlow(int flow) {
        this.flow = flow;
    }

    public void drawArc(Graphics g) {
        g.setColor(Color.getHSBColor(1.0f, 0.75f, 1.0f));
        Point startPoint = start.getCenter();
        Point endPoint = end.getCenter();

        double angle = Math.atan2(endPoint.y - startPoint.y, endPoint.x - startPoint.x);

        int x = (int) (end.getCenter().x - Node.radius * Math.cos(angle));
        int y = (int) (end.getCenter().y - Node.radius * Math.sin(angle));

        drawFlows(g);
        drawArrow(g, start.getCenter(), new Point(x, y));
    }

    private void drawFlows(Graphics g) {
        int x = (start.getCenter().x + end.getCenter().x) / 2;
        int y = (start.getCenter().y + end.getCenter().y) / 2;

        AffineTransform tx = new AffineTransform();

        tx.setToIdentity();
        tx.translate(x, y);
        double angle = Math.atan2(end.getCenter().y - start.getCenter().y, end.getCenter().x - start.getCenter().x);
        tx.rotate(angle);
        String label = flow + "/" + capacity;
        tx.translate(-label.length() * 5, -5);

        Graphics2D g2 = (Graphics2D) g.create();
        g2.setTransform(tx);
        g2.setFont(new Font("TimesRoman", Font.BOLD, 14));
        g2.drawString(label, 0, 0);
        g2.dispose();
    }

    public boolean clicked(Point p) {
        int x = (start.getCenter().x + end.getCenter().x) / 2;
        int y = (start.getCenter().y + end.getCenter().y) / 2;

        if (p.distance(x, y) < 14d * (flow + "/" + capacity).length())
            return true;

        return false;
    }

    public static void drawArrow(Graphics g, Point startPoint, Point endPoint) {
        if (startPoint != null) {
            g.setColor(Color.getHSBColor(1.0f, 0.75f, 1.0f));
            drawArrowHead(g, startPoint, endPoint);
            g.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
        }
    }

    public static void drawArrowHead(Graphics g, Point start, Point end) {
        AffineTransform tx = new AffineTransform();

        Polygon arrowHead = new Polygon();
        arrowHead.addPoint(0, arrowSize);
        arrowHead.addPoint(-arrowSize, -arrowSize);
        arrowHead.addPoint(arrowSize, -arrowSize);

        Graphics2D g2 = (Graphics2D) g.create();

        tx.setToIdentity();
        double angle = Math.atan2(end.y - start.y, end.x - start.x);
        double deltaX = arrowSize * Math.cos(angle);
        double deltaY = arrowSize * Math.sin(angle);
        tx.translate(end.x - deltaX, end.y - deltaY);
        tx.rotate((angle - Math.PI / 2d));

        g2.setTransform(tx);
        g2.fill(arrowHead);
        g2.dispose();
    }

    public String toString() {
        return "(" + start.getNumber() + "->" + end.getNumber() + ")";
    }
}
